OSEDS Server with oAuth2.0 authentication 1.x for Drupal 7.x
----------------------------
- OAuth2.0 authentication

Requirements
------------
In addition to the standard Drupal requirements you will need the following to
make use of OSEDS:

- drush 3.1 - http://drupal.org/project/drush
- drush make 2.0 beta 9 - http://drupal.org/project/drush_make
- git - http://git-scm.com


Getting started
---------------
It provides a `build-octopus-video.make` file for building a full Drupal distro including core
patches as well as a copy of the `Octopus video` install profile.

1. Grab the `build-octopus-video.make.make` file from Octopus Video and run:

        $ drush make build-oauthserver.make [directory]

2. Choose the "aouthServer Profile Video" install profile when installing Drupal

Maintainers
-----------
- OSEDS