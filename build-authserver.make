; This file describes the core project requirements for authserver Several
; patches against Drupal core and their associated issue numbers have been
; included here for reference.

api = 2
core = 7.x

projects[drupal][type] = core
projects[drupal][version] = 7.23

; ------- installation profile --------

; Download the authentication server install profile
projects[authserver][type] = profile
projects[authserver][download][type] = git
projects[authserver][download][url] = https://bitbucket.org/oseds/profile-oauth.git
;projects[authserver][download][branch] = 7.x-1.3
