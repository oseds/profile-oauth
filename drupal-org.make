api = 2
core = 7.x

; Modules
projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.3"

projects[devel][subdir] = "contrib"
projects[devel][version] = "1.3"

projects[entity][subdir] = "contrib"
projects[entity][version] = "1.2"

projects[entityreference][subdir] = "contrib"
projects[entityreference][version] = "1.0"

projects[features][subdir] = "contrib"
projects[features][version] = "1.0"

projects[libraries][subdir] = "contrib"
projects[libraries][version] = "2.1"

projects[module_filter][subdir] = "contrib"
projects[module_filter][version] = "1.8"

projects[rules][subdir] = "contrib"
projects[rules][version] = "2.3"

projects[token][subdir] = "contrib"
projects[token][version] = "1.5"

projects[views][subdir] = "contrib"
projects[views][version] = "3.7"

; auth server
projects[oauth2_server][subdir] = "contrib"
projects[oauth2_server][version] = "1.0-rc3"

projects[services][subdir] = "contrib"
projects[services][version] = "3.5"

projects[services_views][subdir] = "contrib"
projects[services_views][version] = "1.0-beta2"

projects[xautoload][subdir] = "contrib"
projects[xautoload][version] = "3.2"

;themes
projects[portal_theme][version] = "1.0"