<?php
/**
 * @file
 * oauthserver_features.features.inc
 */

/**
 * Implements hook_views_api().
 */
function oauthserver_features_views_api() {
  return array("version" => "3.0");
}
